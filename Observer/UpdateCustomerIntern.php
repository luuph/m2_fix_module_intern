<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Intern
 * @author     Extension Team
 * @copyright  Copyright (c) 2021-2022 BSS Commerce Co. ( https://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace bss\Intern\Observer;

use bss\Intern\Model\DataExampleFactory;
use Exception;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class UpdateCustomerIntern implements ObserverInterface
{
    /**
     * @var Magento\Customer\Api\CustomerRepositoryInterface $_customerRepositoryInterface
     */
    protected $_customerRepositoryInterface;

    /**
     * @var bss\Intern\Model\DataExampleFactory $dataIntern
     */
    protected $dataIntern;

    /**
     * @param Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param bss\Intern\Model\DataExampleFactory $dataIntern
     *
     */
    public function __construct(CustomerRepositoryInterface $customerRepositoryInterface, DataExampleFactory
    $dataIntern)
    {
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->dataIntern = $dataIntern;
    }

    /**
     * Short Description
     *
     * @param Observer $observer
     * @return void
     * @throws Exception
     */
    public function validateEmail(Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        $model = $this->dataIntern->create();
        $check = $model->load($customer->getEmail(), 'email');
        $name = $customer->getFirstname() . " " . $customer->getLastname();
        if ($check->getId()) {
            $check->setName($name);
            $check->save();
        }
    }

    /**
     * Short Description
     *
     * @param Observer $observer
     * @return void
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        $this->validateEmail($observer);
    }
}
