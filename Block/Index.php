<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Intern
 * @author     Extension Team
 * @copyright  Copyright (c) 2021-2022 BSS Commerce Co. ( https://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace bss\Intern\Block;

use bss\Intern\Model\DataExampleFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Template;

/**
 * Class Index
 */
class Index extends Template
{
    /**
     * @var DataExampleFactory
     */
    protected $_dataExample;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * Contruct index
     *
     * @param Template\Context $context
     * @param RequestInterface $request
     * @param DataExampleFactory $dataExample
     */
    public function __construct(
        Template\Context $context,
        RequestInterface $request,
        DataExampleFactory $dataExample
    ) {
        parent::__construct($context);
        $this->_dataExample = $dataExample;
        $this->request = $request;
    }

    /**
     * get Internship
     *
     * @return mixed
     */
    public function getInternship()
    {
        $internship = $this->_dataExample->create();
        $id = $this->request->getParam('id');
        return $internship->load($id);
    }

    /**
     * get Name
     *
     * @return mixed|void
     */
    public function getName()
    {
        if (self::getInternship()) {
            return self::getInternship()->getData('name');
        }
    }

    /**
     * get date of birth
     *
     * @return mixed|void
     */
    public function getDateOfBirth()
    {
        if (self::getInternship()) {
            return self::getInternship()->getData('birthday');
        }
    }

    /**
     * get Gender
     *
     * @return mixed|void
     */
    public function getGender()
    {
        if (self::getInternship()) {
            return self::getInternship()->getData('gender');
        }
    }

    /**
     * get Address
     *
     * @return mixed|void
     */
    public function getAddress()
    {
        if (self::getInternship()) {
            return self::getInternship()->getData('address');
        }
    }

    /**
     * get Email
     *
     * @return mixed|void
     */
    public function getEmail()
    {
        if (self::getInternship()) {
            return self::getInternship()->getData('email');
        }
    }
}
