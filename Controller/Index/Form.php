<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Intern
 * @author     Extension Team
 * @copyright  Copyright (c) 2021-2022 BSS Commerce Co. ( https://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace bss\Intern\Controller\Index;

use bss\Intern\Helper\Data;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;

class Form extends Action
{
    /**
     * @var Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @param Magento\Framework\App\Action\Context $context
     * @param Magento\Framework\View\Result\PageFactory $pageFactory
     * @param Magento\Framework\Controller\ResultFactory $result
     * @param bss\Intern\Helper\Data $helper
     */
    public function __construct(Context $context, PageFactory $pageFactory, ResultFactory $result, Data $helper)
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->resultRedirect = $result;
        $this->helper = $helper;
    }

    /**
     * Default customer account page
     *
     * @return void
     */
    public function execute()
    {
        $enable = $this->helper->isEnable();
        if ($enable == 0) {
            $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('/');
            $this->messageManager->addErrorMessage(__('You do not have enough permissions to access this page,
            please contact the administrator!'));
            return $resultRedirect;
        } else {
            return $this->pageFactory->create();
        }
    }
}
