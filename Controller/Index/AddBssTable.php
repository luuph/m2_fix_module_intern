<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Intern
 * @author     Extension Team
 * @copyright  Copyright (c) 2021-2022 BSS Commerce Co. ( https://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace bss\Intern\Controller\Index;

use bss\Intern\Helper\Data;
use bss\Intern\Model\DataExampleFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;

class AddBssTable extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Magento\Framework\Controller\ResultFactory
     */
    protected $_dataExample;

    /**
     * @var bss\Intern\Model\DataExampleFactory
     */
    protected $resultRedirect;
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @param Magento\Framework\App\Action\Context $context
     * @param bss\Intern\Model\DataExampleFactory $dataExample
     * @param Magento\Framework\Controller\ResultFactory $result
     * @param Magento\Framework\App\RequestInterface $request
     * @param bss\Intern\Helper\Data $helper
     */
    public function __construct(
        Context            $context,
        DataExampleFactory $dataExample,
        ResultFactory      $result,
        RequestInterface   $request,
        Data               $helper
    ) {
        parent::__construct($context);
        $this->_dataExample = $dataExample;
        $this->resultRedirect = $result;
        $this->request = $request;
        $this->helper = $helper;
    }

    /**
     * Validate name
     *
     * @param string $name
     * @return boolean
     */
    public function nameValidate($name): bool
    {
        if (!empty($name)) {
            if (strlen($name) > 2 && strlen($name) < 50) {
                return true;
            }
        }
        return false;
    }

    /**
     * Validate gender
     *
     * @param string $gender
     * @return boolean
     */
    public function genderValidate($gender): bool
    {
        if (!empty($gender)) {
            return true;
        }
        return false;
    }

    /**
     * Validate address
     *
     * @param string $address
     * @return boolean
     */
    public function addressValidate($address): bool
    {
        if (!empty($address)) {
            if (strlen($address) > 2 && strlen($address) < 50) {
                return true;
            }
        }
        return false;
    }

    /**
     * Function validate email
     *
     * @param string $email
     * @return bool
     */
    public function emailValidate($email): bool
    {
        if (!empty($email)) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Function execute
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        $enable = $this->helper->isEnable();
        if ($enable == 1) {
            $name = $this->request->getParam('name');
            $dob = $this->request->getParam('dob');
            $gender = $this->request->getParam('gender');
            $address = $this->request->getParam('address');
            $email = $this->request->getParam('email');
            $model = $this->_dataExample->create();

            if ($this->nameValidate($name) == true
                && $this->genderValidate($gender) == true
                && $this->addressValidate($address) == true
                && $this->emailValidate($email) == true
            ) {
                $model->addData([
                    "name" => $name,
                    "dob" => $dob,
                    "gender" => $gender,
                    "address" => $address,
                    "email" => $email,
                ]);
                $model->save();
                $this->messageManager->addSuccessMessage(__('Your data has been saved!'));
            } else {
                $this->messageManager
                    ->addErrorMessage(__('There was an error while saving Intership data, please try again !'));
            }
        } else {
            $this->messageManager->addErrorMessage(__('You do not have enough permissions to access this page, please
            contact the administrator!'));
        }
        $resultRedirect->setPath('*/*/form');
        return $resultRedirect;
    }
}
