<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Intern
 * @author     Extension Team
 * @copyright  Copyright (c) 2021-2022 BSS Commerce Co. ( https://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace bss\Intern\Plugin;

use bss\Intern\Model\DataExampleFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Message\ManagerInterface;

class InternDiscount
{
    /**
     * @var ManagerInterface $messageManager
     */
    private $messageManager;

    /**
     * @var DataExampleFactory $dataIntern
     */
    protected $dataIntern;

    /**
     * @param ManagerInterface $messageManager
     * @param DataExampleFactory $dataIntern
     */
    public function __construct(ManagerInterface $messageManager, DataExampleFactory $dataIntern)
    {
        $this->messageManager = $messageManager;
        $this->dataIntern = $dataIntern;
    }

    /**
     * Function execute
     *
     * @return void
     */

    public function beforeExecute()
    {
        $objectManager = ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model::Session');
        $model = $this->dataIntern->create()->load($customerSession->getCustomer()->getEmail(), 'email');
        if ($customerSession->isLoggedIn()) {
            ($customerSession->getCustomer()->getEmail() == $model->getEmail()) ?
                $this->messageManager->addSuccess(__('You got 50% off for all orders')) : '';
        }
    }
}
