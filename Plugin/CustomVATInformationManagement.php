<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_Intern
 * @author     Extension Team
 * @copyright  Copyright (c) 2021-2022 BSS Commerce Co. ( https://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace bss\Intern\Plugin;

use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\ShippingInformationManagement;

class CustomVATInformationManagement
{
    /**
     * Function before save address information
     *
     * @param ShippingInformationManagement $subject
     * @param ShippingInformationManagement $cartId
     * @param ShippingInformationInterface $addressInformation
     */
    public function afterSaveAddressInformation(ShippingInformationManagement $subject, ShippingInformationManagement
    $cartId, ShippingInformationInterface $addressInformation)
    {
        $shippingAddress = $addressInformation->getShippingAddress();
        $shippingExtensionAttributes = $shippingAddress->getExtensionAttributes();
        if (!empty($shippingExtensionAttributes)) {
            $shippingExtensionAttributes = $shippingAddress->getExtensionAttributes();
            if (!empty($shippingExtensionAttributes)) {
                $shippingAddress->setCustomVat($shippingExtensionAttributes->getCustomVat());
            }
        }
    }
}
